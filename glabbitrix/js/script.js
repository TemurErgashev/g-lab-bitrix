const portfolioCardColors = ['#5D93C6','#4C77A9','#6987B5','#6983B5','#5C94BC','#81A1C6']
let portfolioCard = document.querySelectorAll('.portfolio__item')


function setCardColor() {
  let counterColor = portfolioCardColors.length;

  for (let i = 0; i < portfolioCard.length; i++) {
    counterColor == 0 ? counterColor = portfolioCardColors.length :
    portfolioCard[i].style.background = `${portfolioCardColors[counterColor]}`;
    counterColor--;
  }
}

setCardColor()

/*Hover*/
const btn = document.querySelectorAll('.btn')
let defX = 0
let defY = 0
let curentColor = '#125796'

btn.forEach(element => {
    element.addEventListener('mousemove', anim)
    element.addEventListener('mouseout', animOut)
});

function anim (event) {
    defX = event.offsetX
    defY = event.offsetY
    event.target.style.background = `radial-gradient(circle at ${defX}px ${defY}px, ${curentColor} 0%, #277ECF 80%)`;
}
function animOut () {
    event.target.style.background = `#277ECF`
}

/*Dotnav*/

let dotNavItem = document.querySelectorAll('.dot-nav-item')
let dotNavLi = document.querySelectorAll('.dot-nav-li')
const BLOCKS_POS = getDotItemPos()

function getDotItemPos() {
  let dotArr = []
  for (let i  of dotNavItem) {
    dotArr.push($(i).position().top)
  }
  return dotArr
}

// dotNavItem.addEventListener('scroll', getDotItemPos)


function dotNav() {

  for (let i = 1; i < BLOCKS_POS.length - 1; i++) {
    if(window.pageYOffset >= BLOCKS_POS[i]){// Если текущая прокрутка больше значений блоков
      if(dotNavLi[i-1].classList.contains('uk-active')){// Если предыдущий класс содержит активный класс, мы его удаляем
          dotNavLi[i-1].classList.remove('uk-active')
      }

      if(dotNavLi[i+1].classList.contains('uk-active')){
        dotNavLi[i+1].classList.remove('uk-active')
      }
      dotNavLi[i].classList.add('uk-active')// То мы присваиваем активный класс
    }
  }

  if(window.pageYOffset <= BLOCKS_POS[1]){
    dotNavLi[0].classList.add('uk-active')
    dotNavLi[1].classList.remove('uk-active')
  }else{
    dotNavLi[0].classList.remove('uk-active')
  }

  if(window.pageYOffset >= BLOCKS_POS[BLOCKS_POS.length-1]){
    dotNavLi[BLOCKS_POS.length-1].classList.add('uk-active')
    dotNavLi[BLOCKS_POS.length-2].classList.remove('uk-active')
  }else{
    dotNavLi[BLOCKS_POS.length-1].classList.remove('uk-active')
  }
  // console.log(arr);
  // elem.getBoundingClientRect().top
  // console.log(elem.getBoundingClientRect().top);
  // console.log(window.pageYOffset);
}

window.addEventListener('scroll', dotNav)
